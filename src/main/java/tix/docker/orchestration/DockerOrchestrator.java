package tix.docker.orchestration;


import com.sun.jersey.api.client.ClientResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tix.docker.client.BuildFlag;
import tix.docker.client.DockerClient;
import tix.docker.client.DockerException;
import tix.docker.client.NotFoundException;
import tix.docker.client.model.Container;
import tix.docker.client.model.ContainerConfig;
import tix.docker.client.model.HostConfig;
import tix.docker.client.model.Ports;
import tix.docker.orchestration.model.Credentials;
import tix.docker.orchestration.model.HealthChecks;
import tix.docker.orchestration.model.Id;
import tix.docker.orchestration.model.Ping;
import tix.docker.orchestration.util.Pinger;

import java.io.*;
import java.nio.charset.Charset;
import java.util.*;

import static org.apache.commons.io.IOUtils.closeQuietly;
import static org.apache.commons.io.IOUtils.copyLarge;

/**
 * Orchestrates multiple Docker containers based on
 */
public class DockerOrchestrator {
    public static final FileFilter DEFAULT_FILTER = new FileFilter() {
		@Override
		public boolean accept(File pathname) {
			return false;
		}
	};
	public static final Properties DEFAULT_PROPERTIES = new Properties();

    private static final Logger LOGGER = LoggerFactory.getLogger(tix.docker.orchestration.DockerOrchestrator.class);
    private static final String DOCKER_HOST_ENVIRONMENT_VARIABLE = "DOCKER_HOST";
	private static final int snooze = 1000;

    private final DockerClient dockerDaemon;
	private final DockerClient privateRegistry;
	private final Repo repo;

    private final FileOrchestrator fileOrchestrator;
	private final Set<BuildFlag> buildFlags;
    private final String label;
    private final String[] envs;
    private final String[] binds;

    /**
     * @deprecated Does not support API version.
     */
    @Deprecated
	public DockerOrchestrator(File src, File workDir, File rootDir, String prefix, Credentials credentials) {
		this(defaultDockerClient(), defaultPrivateRegistryClient(), src, workDir, rootDir, prefix, credentials, DEFAULT_FILTER, DEFAULT_PROPERTIES);
	}

    public DockerOrchestrator(DockerClient dockerDaemon, DockerClient privateRegistry, File src, File workDir, File rootDir, String prefix, Credentials credentials, FileFilter filter, Properties properties) {
        this(dockerDaemon, privateRegistry, new tix.docker.orchestration.Repo(dockerDaemon, prefix, src), new FileOrchestrator(workDir, rootDir, filter, properties), null, credentials, EnumSet.noneOf(BuildFlag.class), new String[0], new String[0]);
    }

	public DockerOrchestrator(DockerClient dockerDaemon, DockerClient privateRegistry, File src, File workDir, File rootDir, String prefix, String label, Credentials credentials, FileFilter filter, Properties properties, Set<BuildFlag> buildFlags, String[] envs, String[] binds) {
		this(dockerDaemon, privateRegistry, new tix.docker.orchestration.Repo(dockerDaemon, prefix, src), new FileOrchestrator(workDir, rootDir, filter, properties), label, credentials, buildFlags, envs, binds);
	}

    public static String defaultHost() {
        StringBuilder builder = new StringBuilder("http://");
        if (System.getenv(DOCKER_HOST_ENVIRONMENT_VARIABLE) != null) {
            builder.append(System.getenv("DOCKER_HOST"));
        } else {
            builder.append("127.0.0.1:2375");
        }
        return builder.toString();
    }

    private static DockerClient defaultDockerClient() {
        try {
            return new DockerClient();
        } catch (DockerException e) {
            throw new OrchestrationException(e);
        }
    }

	private static DockerClient defaultPrivateRegistryClient() {
        return null;
    }

	public DockerOrchestrator(DockerClient dockerDaemon, Repo repo, FileOrchestrator fileOrchestrator, Credentials credentials) {
		this(dockerDaemon, null, repo, fileOrchestrator, null, credentials, EnumSet.noneOf(BuildFlag.class), new String[0], new String[0]);
	}

    public DockerOrchestrator(DockerClient dockerDaemon, DockerClient privateRegistry, Repo repo, FileOrchestrator fileOrchestrator, String label, Credentials credentials, Set<BuildFlag> buildFlags, String[] envs, String[] binds) {
	    if (dockerDaemon == null) {
            throw new IllegalArgumentException("docker is null");
        }
        if (repo == null) {
            throw new IllegalArgumentException("repo is null");
        }
	    if (buildFlags == null) {throw new IllegalArgumentException("buildFlags is null");}


        this.dockerDaemon = dockerDaemon;
        this.privateRegistry = privateRegistry;
        this.repo = repo;
        this.fileOrchestrator = fileOrchestrator;
        this.label = label;
        this.envs = envs;
        this.binds = binds;

        if (credentials != null) {
            dockerDaemon.setCredentials(credentials.username, credentials.password, credentials.email);
        }
	    this.buildFlags = buildFlags;
    }

	public void clean() {
		for (Id id : repo.ids(true)) {
			stop(id);
			clean(id);
		}
	}

	private void clean(final Id id) {
		if (id == null) {
			throw new IllegalArgumentException("id is null");
		}
		stop(id);
		LOGGER.info("clean " + id);
		for (Container container : repo.findContainers(id, true)) {
			LOGGER.info("rm " + Arrays.toString(container.getNames()));
			try {
				dockerDaemon.removeContainer(container.getId());
			} catch (DockerException e) {
				throw new OrchestrationException(e);
			}
		}
		String imageId = null;
		try {
			imageId = repo.getImageId(id);
		} catch (NotFoundException e) {
			LOGGER.warn("Image with tag {} not found: {} ", id, e.getMessage());
		} catch (DockerException e) {
			throw new OrchestrationException(e);
		}
		if (imageId != null) {
			LOGGER.info("rmi " + imageId);
			try {
				dockerDaemon.removeImage(imageId);
			} catch (DockerException e) {
				LOGGER.warn(" - " + e.getMessage());
			}
		}
		snooze();
	}

	private void build(final Id id) {
		if (id == null) {
			throw new IllegalArgumentException("id is null");
		}
		LOGGER.info("package " + id);
		try {
			build(prepare(id), id);
		} catch (IOException e) {
			throw new OrchestrationException(e);
		}

		snooze();
	}

	private void snooze() {
		LOGGER.info("snoozing for " + snooze + "ms");
		try {
			Thread.sleep(snooze);
		} catch (InterruptedException e) {
			throw new OrchestrationException(e);
		}
	}

    private File prepare(Id id) throws IOException {
        if (id == null) {
			throw new IllegalArgumentException("id is null");
		}
        return fileOrchestrator.prepare(id, repo.src(id), repo.conf(id));
    }




	@SuppressWarnings(("DM_DEFAULT_ENCODING"))
	private void build(File dockerFolder, Id id) {

		final ClientResponse response;
		try {
            String tag = repo.imageName(id, privateRegistry);

            response = dockerDaemon.build(dockerFolder, tag, buildFlags);
		} catch (DockerException e) {
			throw new OrchestrationException(e);
		}

		final StringWriter out = new StringWriter();
		try {
			copyLarge(new InputStreamReader(response.getEntityInputStream(), Charset.defaultCharset()), out);
		} catch (IOException e) {
			throw new OrchestrationException(e);
		} finally {
            if (response != null) {
                closeQuietly(response.getEntityInputStream());
            }
        }

		String log = out.toString();
		if (!log.contains("Successfully built")) {
			throw new IllegalStateException("failed to build, log missing lines in" + log);
		}

		// imageId
		// return substringBetween(log, "Successfully built ", "\\n\"}").trim();

		snooze();
	}


    private void start(final Id id) {
        if (id == null) {
            throw new IllegalArgumentException("id is null");
        }

        try {
            LOGGER.info("Started request received for Image: " + id);
            stopRunningContainers(id);
            killRunningContainers(id);
            removeOldContainers(id, 0);
            startContainer(createNewContainer(id), id);
        } catch (DockerException e) {
            throw new OrchestrationException(e);
        }
	    snooze();
	    healthCheck(id);
    }

    private void removeOldContainers(Id id, int keep) throws DockerException {
        List<Container> containers = repo.findContainers(id, true);
        Collections.sort(containers, new Comparator<Container>() {
            @Override
            public int compare(Container o1, Container o2) {
                return -1 * Long.compare(o1.getCreated(), o2.getCreated());
            }
        });
        for (int i = keep ; i < containers.size(); i++) {
            Container container = containers.get(i);
            LOGGER.info("removing container: " + container.getId());
            dockerDaemon.removeContainer(containers.get(i).getId());
        }
    }

     private void killRunningContainers(Id id) throws DockerException {
        for (Container c : repo.findContainers(id, false)) {
            LOGGER.info("killing container: " + c.getId());
            dockerDaemon.kill(c.getId());
        }
    }

    private void stopRunningContainers(Id id) throws DockerException {
        for (Container c : repo.findContainers(id, false)) {
            LOGGER.info("stopping container: " + c.getId());
            dockerDaemon.stopContainer(c.getId());
        }
    }

    private void startContainer(String idOfContainerToStart, final Id id) {
        try {
            LOGGER.info("starting " + id);
            dockerDaemon.startContainer(idOfContainerToStart, newHostConfig(id));
        } catch (DockerException e) {
            LOGGER.error("Unable to start container " + idOfContainerToStart, e);
            throw new OrchestrationException(e);
        }
    }


    private String createNewContainer(Id id) throws DockerException {
        LOGGER.info("creating " + id);
        final ContainerConfig config = new ContainerConfig();
        config.setImage(repo.getImageId(id, privateRegistry));
        config.setEnv(envs);

		String newContainerId = dockerDaemon.createContainer(config, repo.containerName(id)).getId();
		snooze();
        return newContainerId;
	}



	private boolean isRunning(Id id) {
		if (id == null) {throw new IllegalArgumentException("id is null");}
		boolean running = false;
		for (Container container : dockerDaemon.listContainers(false)) {
			final Container candidate = repo.findContainer(id);
			running |= candidate != null && candidate.getId().equals(container.getId());
		}
		return running;
	}

	private void healthCheck(Id id) {
		final HealthChecks healthChecks = repo.conf(id).getHealthChecks();
		for (Ping ping : healthChecks.getPings()) {
			LOGGER.info("pinging " + ping.getUrl());
			if (!Pinger.ping(ping.getUrl(), ping.getTimeout())) {
				throw new OrchestrationException("timeout waiting for " + ping.getUrl() + " for " + ping.getTimeout());
			}
		}
	}

    private List<Id> volumesFrom(Id id) {
		final List<Id> ids = new ArrayList<Id>();
		for (Id from : repo.conf(id).getVolumesFrom()) {
			ids.add(new Id(repo.findContainer(from).getId()));
		}

		return ids;
	}

	private HostConfig newHostConfig(Id id) {
		final HostConfig config = new HostConfig();

		config.setPublishAllPorts(true);
		config.setLinks(links(id));
        config.setBinds(binds);

		LOGGER.info(" - links " + repo.conf(id).getLinks());

		final Ports portBindings = new Ports();
		for (String e : repo.conf(id).getPorts()) {

			final String[] split = e.split(" ");

			assert split.length == 1 || split.length == 2;

			final int a = Integer.parseInt(split[0]);
			final int b = split.length == 2 ? Integer.parseInt(split[1]) : a;

			LOGGER.info(" - port " + e);
			portBindings.addPort(new Ports.Port("tcp", String.valueOf(a), null, String.valueOf(b)));
		}

		config.setPortBindings(portBindings);

		return config;
	}

	private String[] links(Id id) {

		final List<Id> links = repo.conf(id).getLinks();
		final String[] out = new String[links.size()];
		for (int i = 0; i < links.size(); i++) {
			final String name = repo.findContainer(links.get(i)).getNames()[0];
			out[i] = name + ":" + name;
		}

		return out;
	}

	private void stop(final Id id) {
		if (id == null) {
			throw new IllegalArgumentException("id is null");
		}
		for (Container container : repo.findContainers(id, false)) {
			LOGGER.info("stopping " + Arrays.toString(container.getNames()));
			try {
				dockerDaemon.stopContainer(container.getId(), 1);
			} catch (DockerException e) {
				throw new OrchestrationException(e);
			}
			snooze();
		}
	}

	public void build() {
		for (Id id : ids()) {
			build(id);
		}
	}

	public void start() {
		for (Id id : ids()) {
			try {
				if (!repo.imageExists(id, privateRegistry)) {
					build(id);
				}
			} catch (DockerException e) {
				throw new OrchestrationException(e);
			}
			start(id);
		}
	}

	public void stop() {
		for (Id id : repo.ids(true)) {
			stop(id);
		}
	}

	public List<Id> ids() {
		return repo.ids(false);
	}

	public void push() {
		for (Id id : ids()) {
			push(id);
		}
	}

	private void push(Id id) {
		try {
            String name = repo.imageName(id, privateRegistry);
            LOGGER.info("Resolved image ID [" + id + "] to image name [" + name + "]. Preparing to push to " + dockerDaemon.toString());
            dockerDaemon.push(name, label);
		} catch (DockerException e) {
			throw new OrchestrationException(e);
		}
		snooze();
	}

	public boolean isRunning() {
		for (Id id : ids()) {
			if (!isRunning(id)) {
				return false;
			}
		}
		return true;
	}

    public void tag() {
        if (label == null) {
            throw new OrchestrationException("No label set, please supply the docker.label system property");
        }
        try {
            LOGGER.info("Tagging images with label: " + label);
            for (Id id : ids()) {
                String name = repo.imageName(id, privateRegistry);
                LOGGER.info("Resolved image ID [" + id + "} to image name [" + name + "}. Preparing to tag to " + dockerDaemon.toString());
                dockerDaemon.tag(name, name, label);
            }
        } catch (DockerException e) {
            throw new OrchestrationException(e);
        }
    }

    public void untag() {
        if (label == null) {
            throw new OrchestrationException("No label set, please supply the docker.label system property");
        }
        try {
            LOGGER.info("Untagging images with label: " + label);
            for (Id id : ids()) {
                String name = repo.imageName(id, privateRegistry);
                LOGGER.info("Resolved image ID [" + id + "} to image name [" + name + "}. Preparing to remove tag from " + dockerDaemon.toString());
                dockerDaemon.removeImage(name + ":" + label);
            }
        } catch (DockerException e) {
            throw new OrchestrationException(e);
        }
    }
}
