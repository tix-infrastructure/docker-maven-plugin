/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright (c) 2010-2013 Oracle and/or its affiliates. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common Development
 * and Distribution License("CDDL") (collectively, the "License").  You
 * may not use this file except in compliance with the License.  You can
 * obtain a copy of the License at
 * http://glassfish.java.net/public/CDDL+GPL_1_1.html
 * or packager/legal/LICENSE.txt.  See the License for the specific
 * language governing permissions and limitations under the License.
 *
 * When distributing the software, include this License Header Notice in each
 * file and include the License file at packager/legal/LICENSE.txt.
 *
 * GPL Classpath Exception:
 * Oracle designates this particular file as subject to the "Classpath"
 * exception as provided by Oracle in the GPL Version 2 section of the License
 * file that accompanied this code.
 *
 * Modifications:
 * If applicable, add the following below the License Header, with the fields
 * enclosed by brackets [] replaced by your own identifying information:
 * "Portions Copyright [year] [name of copyright owner]"
 *
 * Contributor(s):
 * If you wish your version of this file to be governed by only the CDDL or
 * only the GPL Version 2, indicate your decision by adding "[Contributor]
 * elects to include this software in this distribution under the [CDDL or GPL
 * Version 2] license."  If you don't indicate a single choice of license, a
 * recipient has the option to distribute your version of this file under
 * either the CDDL, the GPL Version 2 or to extend the choice of license to
 * its licensees as provided above.  However, if you add GPL Version 2 code
 * and therefore, elected the GPL Version 2 license, then the option applies
 * only if the new code is made subject to such option by the copyright
 * holder.
 */
package tix.docker.client.utils;

import com.sun.jersey.api.client.*;
import com.sun.jersey.api.client.filter.ClientFilter;

import javax.ws.rs.core.MultivaluedMap;
import java.io.*;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

/**
 * A logging filter.
 *
 * @author Paul.Sandoz@Sun.Com
 */
public class LoggingFilter extends ClientFilter {

    private static final Logger LOGGER = Logger.getLogger(LoggingFilter.class.getName());

    private static final String NOTIFICATION_PREFIX = "* ";

    private static final String REQUEST_PREFIX = "> ";

    private static final String RESPONSE_PREFIX = "< ";

    private final class Adapter extends AbstractClientRequestAdapter {
        private final StringBuilder b;

        Adapter(ClientRequestAdapter cra, StringBuilder b) {
            super(cra);
            this.b = b;
        }

        public OutputStream adapt(ClientRequest request, OutputStream out) throws IOException {
            return new LoggingOutputStream(getAdapter().adapt(request, out), b);
        }

    }

    private final class LoggingOutputStream extends OutputStream {
        private final OutputStream out;

        private final ByteArrayOutputStream baos = new ByteArrayOutputStream();

        private final StringBuilder b;

        LoggingOutputStream(OutputStream out, StringBuilder b) {
            this.out = out;
            this.b = b;
        }

        @Override
        public void write(byte[] b)  throws IOException {
            baos.write(b);
            out.write(b);
        }

        @Override
        public void write(byte[] b, int off, int len)  throws IOException {
            baos.write(b, off, len);
            out.write(b, off, len);
        }

        @Override
        public void write(int b) throws IOException {
            baos.write(b);
            out.write(b);
        }

        @Override
        public void close() throws IOException {
            byte[] entity = baos.toByteArray();
            if (entity.length < 512) {
                printEntity(b, entity);
            } else {
                logger.info("** Not printing entity with size " + entity.length);
            }
            log(b);
            out.close();
        }
    }

    private final PrintStream loggingStream;
    private final Logger logger;
    private final int maxEntitySize;

    private long _id = 0;

    /**
     * Create a logging filter logging the request and response to
     * a default JDK logger, named as the fully qualified class name of this
     * class.
     */
    public LoggingFilter() {
        this(LOGGER);
    }

    public LoggingFilter(int maxEntitySize) {
        this(LOGGER, maxEntitySize);
    }

    /**
     * Create a logging filter logging the request and response to
     * a JDK logger.
     *
     * @param logger the logger to log requests and responses.
     */
    public LoggingFilter(Logger logger) {
        this(logger, null);
    }

    /**
     * Create a logging filter logging the request and response to
     * print stream.
     *
     * @param loggingStream the print stream to log requests and responses.
     */
    public LoggingFilter(PrintStream loggingStream) {
        this(null, loggingStream);
    }

    /**
     * Create a logging filter logging the request and response to
     * a JDK logger.
     *
     * @param logger the logger to log requests and responses.
     * @param maxEntitySize maximum number of entity bytes to be logged (and buffered) - if the entity is larger,
     *                      logging filter will print (and buffer in memory) only the specified number of bytes
     *                      and print "...more..." string at the end.
     */
    public LoggingFilter(Logger logger, int maxEntitySize) {
        this(logger, null, maxEntitySize);
    }

    /**
     * Create a logging filter logging the request and response to
     * print stream.
     *
     * @param loggingStream the print stream to log requests and responses.
     * @param maxEntitySize maximum number of entity bytes to be logged (and buffered) - if the entity is larger,
     *                      logging filter will print (and buffer in memory) only the specified number of bytes
     *                      and print "...more..." string at the end.
     */
    public LoggingFilter(PrintStream loggingStream, int maxEntitySize) {
        this(null, loggingStream, maxEntitySize);
    }

    private LoggingFilter(Logger logger, PrintStream loggingStream) {
        this(logger, loggingStream, 10 * 1024);
    }

    private LoggingFilter(Logger logger, PrintStream loggingStream, int maxEntitySize) {
        this.loggingStream = loggingStream;
        this.logger = logger;
        this.maxEntitySize = maxEntitySize;
    }

    private void log(StringBuilder b) {
        if (logger != null) {
            logger.info(b.toString());
        } else {
            loggingStream.print(b);
        }
    }

    private StringBuilder prefixId(StringBuilder b, long id) {
        b.append(Long.toString(id)).append(" ");
        return b;
    }

    @Override
    public ClientResponse handle(ClientRequest request) throws ClientHandlerException {
        long id = ++this._id;

        logRequest(id, request);

        ClientResponse response = getNext().handle(request);

        logResponse(id, response);

        return response;
    }

    private void logRequest(long id, ClientRequest request) {
        StringBuilder b = new StringBuilder();

        printRequestLine(b, id, request);
        printRequestHeaders(b, id, request.getHeaders());

//        logger.info("Request.getEntity exists: " + (request.getEntity() != null));
        if (request.getEntity() != null) {
            request.setAdapter(new Adapter(request.getAdapter(), b));
        } else {
            log(b);
        }
    }

    private void printRequestLine(StringBuilder b, long id, ClientRequest request) {
        prefixId(b, id).append(NOTIFICATION_PREFIX).append("Client out-bound request").append("\n");
        prefixId(b, id).append(REQUEST_PREFIX).append(request.getMethod()).append(" ").
                append(request.getURI().toASCIIString()).append("\n");
    }

    private void printRequestHeaders(StringBuilder b, long id, MultivaluedMap<String, Object> headers) {
        for (Map.Entry<String, List<Object>> e : headers.entrySet()) {
            List<Object> val = e.getValue();
            String header = e.getKey();

            if(val.size() == 1) {
                String headerValue = ClientRequest.getHeaderValue(val.get(0));
                prefixId(b, id).append(REQUEST_PREFIX).append(header).append(": ").append(headerValue).append("\n");
            } else {
                StringBuilder sb = new StringBuilder();
                boolean add = false;
                for(Object o : val) {
                    if(add) {
                        sb.append(',');
                    }
                    add = true;
                    String headerValue = ClientRequest.getHeaderValue(o);
                    sb.append(headerValue);
                }
                prefixId(b, id).append(REQUEST_PREFIX).append(header).append(": ").append(sb.toString()).append("\n");
            }
        }
    }

    private String sanitizeHeaderValue(String header, String headerValue) {
        if (header.equalsIgnoreCase("Content-Type") ) {
            if (headerValue.equalsIgnoreCase("application/tar")) {
                return "** tar-file of " + headerValue.length() + " bytes **";
            }
        }
        return headerValue;
    }

    private void logResponse(long id, ClientResponse response) {
        StringBuilder b = new StringBuilder();

        printResponseLine(b, id, response);
        printResponseHeaders(b, id, response.getHeaders());

        InputStream stream = response.getEntityInputStream();
        try {
            if (!response.getEntityInputStream().markSupported()) {
                stream = new BufferedInputStream(stream);
                response.setEntityInputStream(stream);
            }

            stream.mark(maxEntitySize + 1);
            byte[] entity = new byte[maxEntitySize + 1];
            int entitySize = stream.read(entity);

            if (entitySize > 0) {
                b.append(new String(entity, 0, Math.min(entitySize, maxEntitySize)));
                if (entitySize > maxEntitySize) {
                    b.append("...more...");
                }
                b.append('\n');
                stream.reset();
            }
        } catch (IOException ex) {
            throw new ClientHandlerException(ex);
        }
        log(b);
    }

    private void printResponseLine(StringBuilder b, long id, ClientResponse response) {
        prefixId(b, id).append(NOTIFICATION_PREFIX).
                append("Client in-bound response").append("\n");
        prefixId(b, id).append(RESPONSE_PREFIX).
                append(Integer.toString(response.getStatus())).
                append("\n");
    }

    private void printResponseHeaders(StringBuilder b, long id, MultivaluedMap<String, String> headers) {
        for (Map.Entry<String, List<String>> e : headers.entrySet()) {
            String header = e.getKey();
            for (String value : e.getValue()) {
                prefixId(b, id).append(RESPONSE_PREFIX).append(header).append(": ").
                        append(value).append("\n");
            }
        }
        prefixId(b, id).append(RESPONSE_PREFIX).append("\n");
    }

    private void printEntity(StringBuilder b, byte[] entity) throws IOException {
        if (entity.length == 0)
            return;
        b.append(new String(entity)).append("\n");
    }
}