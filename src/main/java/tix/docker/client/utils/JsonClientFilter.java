package tix.docker.client.utils;


import com.sun.jersey.api.client.ClientRequest;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.filter.ClientFilter;

import javax.ws.rs.core.MultivaluedMap;

/**
 *
 * @author Konstantin Pelykh (kpelykh@gmail.com)
 *
 */
public class JsonClientFilter extends ClientFilter {

    private static final String CONTENT_TYPE_HEADER = "Content-Type";

    public ClientResponse handle(ClientRequest cr) {
        // Call the next filter
        ClientResponse resp = getNext().handle(cr);
        String respContentType = resp.getHeaders().getFirst(CONTENT_TYPE_HEADER);
        if (respContentType != null && respContentType.startsWith("text/plain")) {
            String newContentType = "application/json" + respContentType.substring(10);
            replaceContentType(resp, newContentType);
        }
        return resp;
    }

    private void replaceContentType(ClientResponse resp, String newContentType) {
        MultivaluedMap<String, String> headers = resp.getHeaders();
        headers.remove(CONTENT_TYPE_HEADER);
        headers.putSingle(CONTENT_TYPE_HEADER, newContentType);
    }
}
