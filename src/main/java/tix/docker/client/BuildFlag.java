package tix.docker.client;

public enum BuildFlag {
	NO_CACHE,
	REMOVE_INTERMEDIATE_IMAGES
}
