package tix.docker.client.model;

/**
 * Created by ben on 12/12/13.
 */
public interface IBuilder<T> {

    T build();
}
