package tix.docker.mojo;

import tix.docker.orchestration.DockerOrchestrator;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;


/**
 * Deploy containers
 */
@Mojo(name = "deploy", defaultPhase = LifecyclePhase.DEPLOY)
public class DeployMojo extends AbstractDockerMojo {

	@Override
	protected void doExecute(DockerOrchestrator orchestrator) {
		orchestrator.push();
	}
}
