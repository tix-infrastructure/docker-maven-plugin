package tix.docker.mojo;

import org.apache.maven.plugins.annotations.Mojo;
import tix.docker.orchestration.DockerOrchestrator;

/**
 * This mojo is responsible for removing a tag. The name of the tag must be supplied
 * using the docker.label system property.
 */
@Mojo(name = "untag")
public class UntagMojo extends AbstractDockerMojo {

    @Override
    protected void doExecute(DockerOrchestrator orchestrator) throws Exception {
        orchestrator.untag();
    }
}
