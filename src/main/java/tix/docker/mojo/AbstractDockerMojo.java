package tix.docker.mojo;

import tix.docker.orchestration.DockerOrchestrator;
import tix.docker.orchestration.model.Credentials;
import tix.docker.orchestration.util.TextFileFilter;
import tix.docker.util.MavenLogAppender;
import tix.docker.client.BuildFlag;
import tix.docker.client.DockerClient;
import tix.docker.client.DockerException;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Component;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;

import java.io.File;
import java.net.URI;
import java.util.EnumSet;
import java.util.Properties;
import java.util.Set;

abstract class AbstractDockerMojo extends AbstractMojo {

    /**
     * When set to true this parameter skips executions of any Mojo of this plugin.
     * This setting is typically used in an environment where docker is not available.
     */
    @Parameter(property = "docker.skip")
    private String skip;

	/**
	 * The host, e.g. -Ddocker.host=http://127.0.0.1:4243
	 */
	@Parameter(property = "docker.host")
	private URI host;

	/**
	 * The private registry, e.g. -Ddocker.registry=http://127.0.0.1:5000
     *
     * In case you're running a local private registry inside a docker, use
     * "boot2docker ip" to find the IP, typically something like 192.168.59.103.
     *
     * By default a docker registry runs at port 5000.
     *
     * Alternatively, you could setup port forwarding on your boot2docker vagrant image
     * so you can access the registry through http://127.0.0.1:5000
	 */
	@Parameter(property = "docker.registry")
	private URI registry;

	/**
	 * A prefix to namespace scope machine. Important for isolating machines.
	 */
	@Parameter(defaultValue = "${project.artifactId}", property = "docker.prefix")
	private String prefix;

    /**
     * The label of the image, used for tagging.
     */
    @Parameter(property = "docker.label")
    private String label;

	/**
	 * Where to look for directories containing Dockerfile and conf.yml
	 */
	@Parameter(defaultValue = "src/main/docker", property = "docker.src")
	private String src;

	/**
	 * Installed Docker version.
	 */
	@Parameter(property = "docker.version")
	private String version;

	/**
	 * Docker username (for deploy).
	 */
	@Parameter(property = "docker.username")
	private String username;

	/**
	 * Docker username (for deploy).
	 */
	@Parameter(property = "docker.password")
	private String password;

	/**
	 * Docker email (for deploy).
	 */
	@Parameter(property = "docker.email")
	private String email;

    /**
     * Docker environment variables (for creating a container).
     */
    @Parameter(property = "docker.env")
    private String env;

    /**
     * Docker binding definitions (for mountinng a host directory as a data volume in a docker)
     */
    @Parameter(property = "docker.binds")
    private String binds;

	/**
	 * Remove intermediate images during build.
	 */
	@Parameter(defaultValue = "false", property = "docker.removeIntermediateImages")
	private boolean removeIntermediateImages;

	@Component
	private MavenProject project;

    @Override
    public final void execute() throws MojoExecutionException, MojoFailureException {

        MavenLogAppender.setLog(getLog());

        if (skip != null) {
            getLog().info("Skipping Docker");
            return;
        }

        // not great eh
        final Properties properties = properties();

        getLog().info("properties filtering supported for " + properties.keySet());

        try {
            final DockerClient docker = dockerClient();
            final DockerClient privateRegistryClient = privateRegistryClient();
            getLog().info("Docker version " + docker.version().getVersion());
            doExecute(new DockerOrchestrator(docker, privateRegistryClient, src(), workDir(), projDir(), prefix, label, credentials(),
                    TextFileFilter.INSTANCE, properties, buildFlags(), env(), binds()));
        } catch (Exception e) {
            throw new MojoExecutionException(e.getMessage(), e);
        }
    }

    private String[] env() {
        if (env == null) {
            return new String[0];
        }
        return env.split(",");
    }

    private String[] binds() {
        if (binds == null) {
            return new String[0];
        }
        return binds.split(",");
    }

	private Set<BuildFlag> buildFlags() {
		return removeIntermediateImages ? EnumSet.of(BuildFlag.REMOVE_INTERMEDIATE_IMAGES) : EnumSet.noneOf(BuildFlag.class);
	}

	private DockerClient dockerClient() throws DockerException {
        String serverUrl = host != null ? host.toString() : DockerOrchestrator.defaultHost();
        return version != null
                ? new DockerClient(serverUrl, version)
                : new DockerClient(serverUrl);
    }

	private DockerClient privateRegistryClient() throws DockerException {
        if (registry == null) {
            return null;
        }
        return version != null
                ? new DockerClient(registry.toString(), version)
                : new DockerClient(registry.toString());
    }

    private Credentials credentials() {
        return (username != null || password != null || email != null) ? new Credentials(username, password, email) : null;
    }

    private Properties properties() {
        final Properties p = new Properties();

        final String[] x = new String[]{
                "project.groupId", project.getGroupId(),
                "project.artifactId", project.getArtifactId(),
                "project.version", project.getVersion(),
                "project.name", project.getName(),
                "project.description", project.getDescription(),
                "project.build.finalName", project.getBuild().getFinalName()
        };

        for (int i = 0; i < x.length; i += 2) {
            if (x[i + 1] != null) {
                p.setProperty(x[i], x[i + 1]);
            }
        }

        p.putAll(project.getProperties());

        return p;
    }

    private File workDir() {
        return new File(project.getBuild().getDirectory(), "docker");
    }

    private File projDir() {
        return project.getBasedir();
    }

    private File src() {
        return new File(projDir(), src);
    }

    protected abstract void doExecute(DockerOrchestrator orchestrator) throws Exception;
}
