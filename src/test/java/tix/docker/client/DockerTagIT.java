package tix.docker.client;

import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import tix.docker.client.model.Image;

import java.util.Arrays;
import java.util.List;

import static org.testng.Assert.fail;

public class DockerTagIT {

    private static final String DOCKER_SERVER_URL = "http://localhost:2375";
    private static final String IMAGE_ID = "0e061e9eded7d6efa222284b8e382a4ae6bd990d3b103826ffd4881cc04ba86e";
    private static final String IMAGE_NAME = "10.250.1.139:5000/employee-app_mongo";
    private static final String TAG = "28-de20247992af0f949ae8df4fa9a37e4a03d7063e";

    private DockerClient dockerClient;

    @BeforeTest
    public void beforeTest() throws DockerException {
        dockerClient = new DockerClient(DOCKER_SERVER_URL);
    }

    @AfterTest
    public void afterTest() throws DockerException {
        dockerClient.removeImage(IMAGE_NAME + ":" + TAG);
    }

    @Test
    public void testStart() throws DockerException, InterruptedException {
        dockerClient.tag(IMAGE_ID, IMAGE_NAME, TAG);
        assertImageExistsWithTag(dockerClient.getImages(), IMAGE_ID, IMAGE_NAME + ":" + TAG);
    }

    private void assertImageExistsWithTag(List<Image> images, String imageId, String tag) {
        for (Image image : images) {
            if (image.getId().equals(imageId)) {
                if (Arrays.asList(image.getRepoTags()).contains(tag)) {
                    return;
                }
            }
        }
        fail(String.format("No image exists with ID %s and tag %s", imageId, tag));
    }
}
