package tix.docker.client;

import tix.docker.client.model.Container;
import tix.docker.client.model.ContainerConfig;
import tix.docker.client.model.ContainerCreateResponse;
import tix.docker.client.model.HostConfig;
import org.testng.Assert;
import org.testng.annotations.*;

public class DockerStartContainerWithLinkIT extends Assert {

    private static final String DOCKER_SERVER_URL = "http://localhost:2375";
    private static final String IMAGE_ID = "218df16835f002a578bcff238034bd9fa43c76efaf245b053694913e7eb10c85";
    private static final String CONTAINER_NAME = "reader30_app";
    private static final String LINK = "/reader30_mongo:/reader30_mongo";

    private DockerClient dockerClient;
    private String containerId;

    @BeforeTest
    public void beforeTest() throws DockerException {
        dockerClient = new DockerClient(DOCKER_SERVER_URL);
        containerId = createContainer(IMAGE_ID, CONTAINER_NAME);
    }

    @AfterTest
    public void afterTest() throws DockerException {
        if (isRunning(findContainer(containerId))) {
            dockerClient.stopContainer(containerId);
        }
        dockerClient.removeContainer(containerId);
    }

    @Test
    public void testStart() throws DockerException, InterruptedException {
        startContainer(containerId, LINK);

        Thread.sleep(1000);

        Container container = findContainer(containerId);
        assertTrue(isRunning(container), "Actual state: " + container.getStatus());
    }

    private boolean isRunning(Container container) {
        return container.getStatus().startsWith("Up");
    }

    private void startContainer(String containerId, String link) throws DockerException {
        HostConfig hostConfig = new HostConfig();
        hostConfig.setLinks(new String[] { link });

        dockerClient.startContainer(containerId, hostConfig);
    }

    private Container findContainer(String containerId) {
        for (Container container : dockerClient.listContainers(true)) {
            if (container.getId().equals(containerId)) {
                return container;
            }
        }
        return null;
    }

    private String createContainer(String imageId, String name) throws DockerException {
        ContainerConfig config = new ContainerConfig();
        config.setImage(imageId);
        ContainerCreateResponse response = dockerClient.createContainer(config, name);
        return response.getId();
    }
}
