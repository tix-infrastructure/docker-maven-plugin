Combines several modules from https://github.com/alexec/docker-maven-plugin

1. Support API version 1.14
1. Added support for tagging from a continuous delivery pipeline using docker.label system property
1. Added docker.skip propert to skip any docker goals
1. Similar to CLI, Maven plugin now respects the DOCKER_HOST environment variable
1. Added support for running integration tests and exporting results.
1. Allow environment variables and binding definitions to be supplied when starting a new container
1. Stops and removes any running and/or existing containers with the same image name
1. Removed timeout because some Docker actions may take a long time (run integration and/or performance tests)
1. Instead of having 3 modules, now all-in-one.
1. Artifactory distribution mgmt
